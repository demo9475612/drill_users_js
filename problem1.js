//Find all users who are interested in playing video games.

function toFindUserPlayingVideoGames(users){
    

    return Object.entries(users).filter(([ userName, userData])=>{
         
           const userInterests = userData.interests || userData.interest;
           const userInterestArray = userInterests[0].includes("Video Games");
           if(userInterestArray){
             return userName;
           }
          
    }).map(([userName,userData])=>userName); 

}

module.exports=toFindUserPlayingVideoGames;