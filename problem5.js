

function groupUsersBasedOnTheirProgrammingLanguage(users){

        return Object.entries(users).reduce((acc,[userName,userData])=>{
               //const programmingLanguage = 
               const programmingLanguage = userData.desgination.split(" ")[0];
               acc[programmingLanguage] = acc[programmingLanguage] || [];
               acc[programmingLanguage].push(userName);
               return acc;
         },{});
}
module.exports = groupUsersBasedOnTheirProgrammingLanguage;