function userStayingInGermany(users){
      return  Object.entries(users)
      .filter(([userName,userData])=>userData.nationality==='Germany')
      .map(([userName,userData])=>userName);
}
module.exports = userStayingInGermany;