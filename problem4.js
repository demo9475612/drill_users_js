function usersWithMastersDegree(users){
  
         return Object.entries(users).filter(([usersName,userData])=>userData.qualification==='Masters').map(([userName,userData])=>userName);
}
module.exports = usersWithMastersDegree;