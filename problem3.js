function sortUserSeniorityLevel(users){
     const seniorityOrder = { "Senior": 1, "Python": 2, "Intern": 3 };
     return Object.entries(users).sort(([userName1,userData1],[userName2,userData2])=>{
        const seniorityLevel1 = seniorityOrder[userData1.desgination.split(" ")[0]];
        const seniorityLevel2 = seniorityOrder[userData2.desgination.split(" ")[0]];
        if(seniorityLevel1!=seniorityLevel2){
            return seniorityLevel1- seniorityLevel2;
        }else{
            return userData2.age - userData1.age;
        }
     }).map(([userName,userData])=>userName);

}
module.exports = sortUserSeniorityLevel;






  